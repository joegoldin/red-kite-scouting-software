/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spartatroniks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.write.WritableWorkbook;

/**
 *
 * @author Joe Goldin
 */
public class ExcelInterface {

    private Workbook workbook;
    private WritableWorkbook writableWorkbook;
    private File workbookFile;
    private Workbook workbookForSave;
    public static FileOutputStream outputStream;
    private final WorkbookSettings workbookSettings;

    public ExcelInterface() throws IOException, BiffException {
        workbook = Workbook.getWorkbook(new File("data/database.xls"));
        workbookForSave = Workbook.getWorkbook(new File("data/databaseForWriting.xls"));
        workbookFile = new File("data/databaseForWriting.xls");
        outputStream = new FileOutputStream(workbookFile);
        workbookSettings = new WorkbookSettings();
//        workbookSettings.setUseTemporaryFileDuringWrite()
        writableWorkbook = Workbook.createWorkbook(workbookFile, workbook);
    }

    public Workbook getWorkbook() {
        return workbook;
    }

    public Workbook getWorkbookAgain() {
        try {
            workbook = Workbook.getWorkbook(new File("data/database.xls"));
        } catch (IOException ex) {
            Logger.getLogger(ExcelInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BiffException ex) {
            Logger.getLogger(ExcelInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
        return workbook;
    }

    public WritableWorkbook geWritabletWorkbookAgain() {
        workbook = getWorkbookAgain();

        try {

            workbookFile = new File("data/databaseForWriting.xls");
            writableWorkbook = Workbook.createWorkbook(workbookFile, workbook);
        } catch (IOException ex) {
            Logger.getLogger(ExcelInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return writableWorkbook;
    }

    public Workbook getWorkbookForSave() {
        return workbookForSave;
    }

    public WritableWorkbook getWritableWorkbook() {
        return writableWorkbook;
    }

    public FileOutputStream getOutputStream() {
        return outputStream;
    }
}